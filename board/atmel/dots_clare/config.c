/*
 * Copyright (C) 2015 Atmel Corporation
 *		      Wenyou.Yang <wenyou.yang@atmel.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <atmel_lcd.h>
#include <debug_uart.h>
#include <dm.h>
#include <i2c.h>
#include <nand.h>
#include <version.h>
#include <video.h>
#include <asm/io.h>
#include <dm.h>
#include <errno.h>
#include <spi.h>

#include <asm/io.h>
#include <asm/arch/at91_common.h>
#include <asm/arch/atmel_pio4.h>
#include <asm/arch/atmel_mpddrc.h>
#include <asm/arch/atmel_sdhci.h>
#include <asm/arch/clk.h>
#include <asm/arch/gpio.h>
#include <asm/arch/sama5d2.h>
#include <asm/arch/sama5d2_smc.h>

#include "display.h"

DECLARE_GLOBAL_DATA_PTR;


// RWS: SPI init
static void board_spi_init(void)
{
	// Map the correct IOSet to SPI
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 14, 0); 
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 15, 0); 
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 16, 0); 

	// GPIOs for RST and C/D
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 13, ATMEL_PIO_DIR_MASK);
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 22, ATMEL_PIO_DIR_MASK);
	// DISP_EN
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 25, ATMEL_PIO_DIR_MASK);
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 25, 1);
}

static void board_usb_hw_init(void)
{
	atmel_pio4_set_pio_output(AT91_PIO_PORTB, 10, 1);
}

#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{

//	printf("board_late_init\n");
	display_Power_On(0);

	set_Display_Welcome();

	return 0;
}
#endif

#ifdef CONFIG_DEBUG_UART_BOARD_INIT
static void board_uart1_hw_init(void)
{
	atmel_pio4_set_a_periph(AT91_PIO_PORTD, 2, ATMEL_PIO_PUEN_MASK);	/* URXD1 */
	atmel_pio4_set_a_periph(AT91_PIO_PORTD, 3, 0);	/* UTXD1 */

	at91_periph_clk_enable(ATMEL_ID_UART1);
}

void board_debug_uart_init(void)
{
	board_uart1_hw_init();
}
#endif

#ifdef CONFIG_NAND_ATMEL
static void board_nand_hw_init(void)
{
        struct at91_smc *smc = (struct at91_smc *)ATMEL_BASE_SMC;

        at91_periph_clk_enable(ATMEL_ID_HSMC);

        /* Configure SMC CS3 for NAND */
        writel(AT91_SMC_SETUP_NWE(1) | AT91_SMC_SETUP_NCS_WR(1) |
               AT91_SMC_SETUP_NRD(1) | AT91_SMC_SETUP_NCS_RD(1),
               &smc->cs[3].setup);
        writel(AT91_SMC_PULSE_NWE(2) | AT91_SMC_PULSE_NCS_WR(3) |
               AT91_SMC_PULSE_NRD(2) | AT91_SMC_PULSE_NCS_RD(3),
               &smc->cs[3].pulse);
        writel(AT91_SMC_CYCLE_NWE(5) | AT91_SMC_CYCLE_NRD(5),
               &smc->cs[3].cycle);
        writel(AT91_SMC_TIMINGS_TCLR(2) | AT91_SMC_TIMINGS_TADL(7) |
               AT91_SMC_TIMINGS_TAR(2)  | AT91_SMC_TIMINGS_TRR(3)   |
               AT91_SMC_TIMINGS_TWB(7)  | AT91_SMC_TIMINGS_RBNSEL(2)|
               AT91_SMC_TIMINGS_NFSEL(1), &smc->cs[3].timings);
        writel(AT91_SMC_MODE_RM_NRD | AT91_SMC_MODE_WM_NWE |
               AT91_SMC_MODE_EXNW_DISABLE |
               AT91_SMC_MODE_DBW_8 |
               AT91_SMC_MODE_TDF_CYCLE(1),
               &smc->cs[3].mode);

        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  0, ATMEL_PIO_DRVSTR_ME);       /* D0 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  1, ATMEL_PIO_DRVSTR_ME);       /* D1 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  2, ATMEL_PIO_DRVSTR_ME);       /* D2 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  3, ATMEL_PIO_DRVSTR_ME);       /* D3 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  4, ATMEL_PIO_DRVSTR_ME);       /* D4 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  5, ATMEL_PIO_DRVSTR_ME);       /* D5 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  6, ATMEL_PIO_DRVSTR_ME);       /* D6 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  7, ATMEL_PIO_DRVSTR_ME);       /* D7 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,12, 0);  /* RE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  8, 0); /* WE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  9, ATMEL_PIO_PUEN_MASK);       /* NCS */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,21, ATMEL_PIO_PUEN_MASK);        /* RDY */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,10, ATMEL_PIO_PUEN_MASK);        /* ALE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,11, ATMEL_PIO_PUEN_MASK);        /* CLE */
}
#endif


#ifdef CONFIG_BOARD_EARLY_INIT_F
int board_early_init_f(void)
{
#ifdef CONFIG_DEBUG_UART
	debug_uart_init();
#endif

	return 0;
}
#endif

int board_init(void)
{
	/* address of boot parameters */
	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;

#ifdef CONFIG_NAND_ATMEL
	board_nand_hw_init();
#endif

#ifdef CONFIG_CMD_USB
	board_usb_hw_init();
#endif

	board_spi_init();

	return 0;
}

int dram_init(void)
{
	gd->ram_size = get_ram_size((void *)CONFIG_SYS_SDRAM_BASE,
				    CONFIG_SYS_SDRAM_SIZE);
	return 0;
}

#ifdef CONFIG_MISC_INIT_R
int misc_init_r(void)
{
	return 0;
}
#endif

/* SPL */
#ifdef CONFIG_SPL_BUILD
#error "Not ready for spl build"
#endif
